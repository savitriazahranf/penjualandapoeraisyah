<?php

namespace App\Controllers;

use App\Models\MenuModels;

class Menu extends BaseController
{
    protected $menuModels;
    public function __construct()
    {
        // memanggil produk models
        $this->menuModel = new MenuModels();
    }
    public function index()
    {
        $data = [
            'title' => 'Data Menu',
            'produk' => $this->menuModel->getMenu(),
            'validation' => \Config\Services::validation()
        ];
        return view('admin/data-menu', $data);
    }
    public function simpan()
    {
        // validasi input
        if (!$this->validate([
            'id' => [
                'rules' => 'required|is_unique[menu.id]',
                'errors' => [
                    'is_unique' => 'id sudah digunakan',
                    'required' => 'id item menu harus diisi'
                ]
            ],
            'nama' => [
                'rules' => 'required|string',
                'errors' => [
                    'string' => 'field nama harus berisi huruf',
                    'required' => 'nama item menu harus diisi'
                ]
            ],
            'foto' => [
                'rules' => 'uploaded[foto]|max_size[foto,1024]|is_image[foto]|mime_in[foto,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'uploaded' => 'foto produk harus diupload',
                    'max_size' => 'ukuran foto terlalu besar',
                    'is_image' => 'file harus berupa image',
                    'mime_in' => 'file harus berupa jpg, jpeg, png'
                ]
            ],
            'detail' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'field detail item harus diisi'
                ]
            ],
            'harga' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'harga item menu harus diisi'
                ]
            ],
        ])) {
            // menampilkan pesan ketika error pada field
            session()->setFlashdata('error', $this->validator->listErrors());

            return redirect()->to('/data-produk')->withInput();
        }

        // mengambil data foto
        $fotoMenu = $this->request->getFile('foto');

        // menentukan nama secara acak
        $namaMenu = $fotoMenu->getRandomName();

        // memindahkan image ke folder img
        $fotoMenu->move('img', $namaMenu);

        $data = [
            'id' => $this->request->getVar('id'),
            'nama' => $this->request->getVar('nama'),
            'harga' => $this->request->getVar('harga'),
            'foto' => $namaMenu,
            'detail' => $this->request->getVar('detail'),
        ];

        $this->menuModel->addMenu($data);

        // menampilkan pesan ketika data berhasil ditambahkan
        session()->setFlashdata('berhasil', 'Data item menu berhasil ditambahkan');

        return redirect()->to('/data-produk');
    }

    public function ubah()
    {
        if (!$this->validate([
            // validasi input
            'nama' => [
                'rules' => 'required|string',
                'errors' => [
                    'string' => 'field nama harus berisi huruf',
                    'required' => 'nama produk harus diisi'
                ]
            ],
            'harga' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'harga produk harus diisi'
                ]
            ],
            'detail' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'field detail item harus diisi'
                ]
            ],
            'harga' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'harga item menu harus diisi'
                ]
            ],
        ])) {
            // menampilkan data ketika terjadi kesalahan
            session()->setFlashdata('error', $this->validator->listErrors());

            return redirect()->to('/data-menu')->withInput();
        }


        $id = $this->request->getVar('id');

        $data = [
            'nama' => $this->request->getVar('nama'),
            'harga' => $this->request->getVar('harga'),
            'detail' => $this->request->getVar('detail'),
        ];

        // method untuk update data
        $this->menuModel->updateMenu($id, $data);

        // menampilkan pesan ketika data berhasil di ubah
        session()->setFlashdata('berhasil', 'Data produk berhasil diubah!!!');

        return redirect()->to('/data-produk');
    }

    public function hapus()
    {
        // mengambil data id
        $data = [
            'id' => $this->request->getVar('id')
        ];

        // method untuk menghapus data
        $this->menuModel->deleteMenu($data);

        // menampilkan pesan ketika data berhasil dihapus
        session()->setFlashdata('berhasil', 'Produk berhasil dihapus!!!');

        return redirect()->to('/data-produk');
    }
}
//protected $request;  