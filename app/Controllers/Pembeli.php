<?php

namespace App\Controllers;

class Pembeli extends BaseController
{
    public function index()
    {
        $data = [
            'cart' => \Config\Services::cart()
        ];

        return view('pembeli/index', $data);
    }
}
