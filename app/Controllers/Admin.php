<?php

namespace App\Controllers;

use App\Models\MenuModels;
use App\Models\OrderModels;

class Admin extends BaseController
{
    protected $menuModel, $orderModel;
    public function __construct()
    {
        $this->menuModel = new MenuModels();
        $this->orderModel = new OrderModels();
    }

    public function index()
    {
        $data = [
            'validation' =>  \Config\Services::validation(),
            'order' => $this->orderModel->countOrder(),
            'menu' => $this->menuModel->countMenu(),
            'cart' => \Config\Services::cart()
        ];

        return view('admin/index', $data);
    }
}
