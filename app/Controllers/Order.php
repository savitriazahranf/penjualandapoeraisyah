<?php

namespace App\Controllers;

use App\Models\MenuModels;
use App\Models\OrderModels;

class Order extends BaseController
{
    protected $menuModel, $cart, $orderModel;
    public function __construct()
    {
        $this->cart = \Config\Services::cart();
        $this->menuModel = new MenuModels();
        $this->orderModel = new OrderModels();
    }

    public function index()
    {
        $data = [
            'validation' => \Config\Services::validation(),
            'produk' => $this->menuModel->getMenu(),
            'cart' => \Config\Services::cart(),
            'keranjang' => $this->cart->contents(),
        ];

        return view('pembeli/katalog', $data);
    }

    public function addOrder()
    {
        $data = [
            'id' => $this->request->getVar('id'),
            'qty' => 1,
            'price' => $this->request->getVar('harga'),
            'name' => $this->request->getVar('nama'),
            'options' => [
                'foto' => $this->request->getVar('foto'),
                'detail' => $this->request->getVar('detail')
            ]
        ];

        if ($this->cart->insert($data)) {
            # code...
            session()->setFlashdata('berhasil', 'Selamat produk berhasil ditambahkan!!!');

            return redirect()->to('/katalog');
        } else {
            session()->setFlashdata('gagal', 'Maaf produk gagal ditambahkan');

            return redirect()->to('/katalog');
        }
    }

    public function getOrder()
    {
        # code...
        $data = [
            'title' => 'Detail Pesanan',
            'transaksi' => $this->orderModel->getOrder(),
            'cart' => $this->cart
        ];

        return view('pembeli/detail-pesanan', $data);
    }

    public function updateOrder()
    {
        $i = 1;
        foreach ($this->cart->contents() as $value) {
            # code...
            $this->cart->update(array(
                'rowid'   => $value['rowid'],
                'qty'     => $this->request->getVar('qty' . $i++),
            ));
        }

        session()->setFlashdata('berhasil', 'Selamat keranjang berhasil diperbarui');
        return redirect()->to('/detail-pesanan');
    }

    public function deleteOrder($rowid)
    {
        $this->cart->remove($rowid);

        session()->setFlashdata('berhasil', 'Produk berhasil dihapus!');
        return redirect()->to('/detail-pesanan');
    }

    public function checkout()
    {
        $data = [
            'id' => $this->request->getVar('id'),
            'menu_id' => $this->request->getVar('menu_id'),
            'nomor_meja' => $this->request->getVar('nomor_meja'),
            'user_id' => $this->request->getVar('user_id'),
            'jumlah_item' => $this->request->getVar('jumlah_item'),
            'total_harga' => $this->request->getVar('total_harga'),
            'keterangan' => $this->request->getVar('keterangan')
        ];

        if ($this->orderModel->insert($data)) {
            # code...
            session()->setFlashdata('berhasil', 'Pesanan anda segera diproses');

            return redirect()->to('/detail-pesanan');
        } else {
            session()->setFlashdata('gagal', 'Maaf produk gagal ditambahkan');

            return redirect()->to('/detail-pesanan');
        }
    }

    public function getAllOrder()
    {
        $data = [
            'order' => $this->orderModel->getOrder(),
        ];

        return view('admin/data-orderan', $data);
    }
}
