<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-text mx-3">DAPOER AISYAH</div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <?php if (in_groups('admin')) : ?>
        <!-- Heading -->
        <div class="sidebar-heading pt-3">
            Menu
        </div>

        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard-admin'); ?>">
                <i class="fas fa-address-card"></i>
                <span>Dashboard</span></a>
        </li>
        <!-- Nav Item - Product -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('data-produk'); ?>">
                <i class="fas fa-book"></i>
                <span>Product</span></a>
        </li>
        <!-- Nav Item - Orderan -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('data-orderan'); ?>">
                <i class="fas fa-cart-plus"></i>
                <span>Orderan</span></a>
        </li>
        <!-- Nav Item - Pemasukan -->
        <li class="nav-item">
            <a class="nav-link" href="tables.html">
                <i class="fas fa-fw fa-table"></i>
                <span>Pemasukan</span></a>
        </li>
    <?php endif; ?>

    <?php if (in_groups('pembeli')) : ?>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard-pembeli'); ?>">
                <i class="fas fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('katalog'); ?>">
                <i class="fas fa-fw fa-list-alt"></i>
                <span>Katalog</span></a>
        </li>
    <?php endif; ?>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->