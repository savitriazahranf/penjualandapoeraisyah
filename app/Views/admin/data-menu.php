<?= $this->extend('templates/index'); ?>

<?= $this->section('page-section'); ?>

<div class="container-fluid">

    <?php if (session()->getFlashdata('berhasil')) : ?>
        <div class="alert alert-success" role="alert">
            <?= session()->getFlashdata('berhasil'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>
    <?php if (session()->getFlashdata('error')) : ?>
        <div class="alert alert-warning pb-0" role="alert">
            <?= session()->getFlashdata('error'); ?>
        </div>
    <?php endif; ?>

    <!-- Model Tambah Data -->
    <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="Menu/simpan" method="POST" enctype="multipart/form-data">
                        <?= csrf_field(); ?>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Id</label>
                            <input type="text" class="form-control" name="id" readonly value="PRD<?php echo random_string('numeric', 6);  ?>" d>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Nama</label>
                            <input type="text" class="form-control" name="nama" placeholder="Jamur Goreng">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Detail</label>
                            <input type="text" class="form-control" name="detail" placeholder="Jamur Digoreng">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Harga</label>
                            <input type="text" class="form-control" name="harga" placeholder="Rp. 10,000">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Foto</label>
                            <input type="file" class="form-control" name="foto">
                        </div>
                        <div class="form-group mt-4 justify-content-end d-flex">
                            <button type="button" class="btn mx-2 btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn mx-2 btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Model Edit Data -->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="Menu/ubah" method="POST" enctype="multipart/form-data">
                        <?= csrf_field(); ?>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Id</label>
                            <input type="text" class="form-control" id="id" name="id" readonly>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Detail</label>
                            <input type="text" class="form-control" id="detail" name="detail">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Harga</label>
                            <input type="text" class="form-control" id="harga" name="harga">
                        </div>
                        <div class="form-group mt-4 justify-content-end d-flex">
                            <button type="button" class="btn mx-2 btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn mx-2 btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal hapus data produk-->
    <div class="modal fade" id="modalHapus">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="/Menu/hapus/" method="post">
                    <div class="modal-body">
                        Apakah anda yakin menghapus data ini?
                        <input type="hidden" name="id" id="id">
                    </div>
                    <div class="col-12 pt-2 form-group gap-3 d-flex justify-content-end">
                        <button type="button" class="btn btn-secondary mr-3" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Produk</h6>
        </div>

        <div class="card-body">
            <button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target="#modalTambah">Tambah Data</button>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="10">No</th>
                            <th>Id</th>
                            <th>Nama</th>
                            <th>Detail</th>
                            <th width="170">Harga</th>
                            <th width="300">Foto</th>
                            <th width="120">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($produk as $produk) :
                        ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $produk['id']; ?></td>
                                <td><?= $produk['nama']; ?></td>
                                <td><?= $produk['detail']; ?></td>
                                <td><?= number_to_currency($produk['harga'], 'IDR ') ?></td>
                                <td>
                                    <img style="width: 300px; height: 200px; object-fit: cover; border-radius: 1rem;" src="/img/<?= $produk['foto']; ?>" alt="<?= $produk['nama']; ?>">
                                </td>
                                <td>
                                    <button type="button" class="btn btn-warning btn-circle" id="btn-edit-menu" data-toggle="modal" data-target="#modalEdit" data-id="<?= $produk['id']; ?>" data-nama="<?= $produk['nama']; ?>" data-harga="<?= $produk['harga']; ?>" data-detail="<?= $produk['detail']; ?>"><i class="fas fa-edit"></i></button>
                                    <button type="button" class="btn btn-danger mx-1 btn-circle" id="btn-hapus-menu" data-toggle="modal" data-target="#modalHapus" data-id="<?= $produk['id']; ?>">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<?= $this->endSection(); ?>