<?= $this->extend('templates/index'); ?>


<?= $this->section('page-section'); ?>

<div class="container-fluid ">

    <div class="d-flex justify-content-center row">

        <div class="col-md-12 py-4">
            <?php if (session()->getFlashdata('berhasil')) : ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->getFlashdata('berhasil'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>

            <div class="receipt bg-white p-3 rounded">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="mt-2 mb-3">Keranjang Belanja Saya</h4>
                        <h6 class="name">Hello, <?= user()->username; ?> kamu sudah belanja apa hari ini?</h6>
                    </div>
                    <div class="col-md-6 d-flex justify-content-end">
                        <img width="80" height="80" src="/img/default.png" alt="">
                    </div>
                </div>
                <hr>

                <div class="d-flex flex-row justify-content-between align-items-center order-details">
                    <div>
                        <span class="d-block fs-12">Nomor Pemesanan</span><span class="font-weight-bold"><input type="text" name="id" disabled value="<?php echo random_string('numeric', 9);  ?>"></span>
                    </div>
                    <div>
                        <span class="d-block fs-12">Tanggal Order</span><span class="font-weight-bold"><?= date('d F Y'); ?></span>
                    </div>
                    <div>
                        <span class="d-block fs-12">Pembayaran</span><span class="font-weight-bold">Credit card</span><img class="ml-1 mb-1" src="https://i.imgur.com/ZZr3Yqj.png" width="20">
                    </div>
                    <div>
                        <span class="d-block fs-12">Nomor Meja</span><span class="font-weight-bold text-success"><input type="text" name="nomor_meja"></span>
                    </div>
                </div>

                <hr>
                <?php echo form_open('Order/updateOrder');

                ?>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="90px">Gambar</th>
                            <th width="60%">Nama Barang</th>
                            <th>Harga</th>
                            <th width="10px">Qty</th>
                            <th width="70px">Action</th>
                        </tr>
                    </thead>
                    <?php
                    $i = 1;
                    $jml_item = 0;

                    foreach ($cart->contents() as $keranjang) :
                        $jml_item = $jml_item + $keranjang['qty'];

                        $ppn = $cart->total() * 10 / 100;

                        $total = $cart->total() + $ppn;
                    ?>
                        <tbody>
                            <tr>
                                <td>
                                    <img class="rounded" src="/img/<?= $keranjang['options']['foto']; ?>" width="80" height="80">
                                </td>
                                <td class="align-middle">
                                    <h6 class="large font-weight-bold"><?= $keranjang['name']; ?>
                                    </h6>
                                    <sub><?= $keranjang['options']['detail']; ?></sub>
                                </td>
                                <td class="align-middle">
                                    <?= number_to_currency($keranjang['subtotal'], 'IDR ') ?>
                                </td>
                                <td class="align-middle">
                                    <input style="width: 50px;" type="number" min="1" name="qty<?= $i++; ?>" value="<?= $keranjang['qty']; ?>">
                                </td>
                                <td class="align-middle">
                                    <a href="<?= base_url('Order/deleteOrder/' . $keranjang['rowid']); ?>" class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    <?php endforeach; ?>
                </table>

                <div class="mt-5 amount row">
                    <div class="d-flex justify-content-center col-md-6"></div>
                    <div class="col-md-6">
                        <div class="billing">
                            <div class="d-flex justify-content-between"><span>Subtotal</span><span class="font-weight-bold"><?= number_to_currency($cart->total(), 'IDR ') ?></span></div>
                            <div class="d-flex justify-content-between mt-2">
                                <span>PPN 10%</span>
                                <span class="font-weight-bold"><?= number_to_currency($ppn, 'IDR ') ?></span>
                            </div>
                            <hr>
                            <div class="d-flex justify-content-between mt-1">
                                <span class="font-weight-bold">Total</span>
                                <span class="font-weight-bold text-success"><?= number_to_currency($total, 'IDR ') ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="d-flex mt-3 mb-2 justify-content-end">
                    <div class="d-flex">
                        <button class="btn btn-warning mr-3" type="submit"><i class="far fa-save"></i> Update</button>
                        <?php echo form_close(); ?>

                        <!-- Form checkout -->
                        <?php
                        echo form_open('Order/checkout');

                        echo form_hidden('id', random_string('numeric', 9));
                        echo form_hidden("menu_id", $keranjang['id']);
                        echo form_hidden('nomor_meja', random_string('numeric', 2));
                        echo form_hidden('jumlah_item', $jml_item);
                        echo form_hidden('user_id', user()->id);
                        echo form_hidden('total_harga', $total);
                        echo form_hidden("keterangan", $keranjang['name']);
                        ?>

                        <button class="btn btn-primary mr-2" type="submit">
                            <i class="far fa-credit-card"></i> Checkout
                        </button>

                        <?php echo form_close(); ?>
                    </div>
                </div>

                <hr>

                <div class="d-flex justify-content-between align-items-center footer">
                    <div class="thanks">
                        <span class="d-block font-weight-bold">Terima kasih telah berbelanja</span>
                        <span>Dapoer Aisyah</span>
                    </div>
                    <div class="d-flex flex-column justify-content-end align-items-end">
                        <span class="d-block font-weight-bold">Need Help?</span>
                        <span>+62 123-3231-5830</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>