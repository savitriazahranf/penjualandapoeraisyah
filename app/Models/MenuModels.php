<?php

namespace App\Models;

use CodeIgniter\Model;

class MenuModels extends Model
{
    protected $table = 'menu';
    protected $allowedFields = ['id', 'nama', 'detail', 'foto', 'harga', 'created_at', 'updated_at'];
    protected $useTimestamps = true;
    protected $primaryKey = 'id';

    public function getMenu()
    {
        return $this->findAll();
    }

    public function addMenu($data)
    {
        return $this->insert($data);
    }

    public function updateMenu($data, $id)
    {
        return $this->update($data, $id);
    }

    public function deleteMenu($data)
    {
        return $this->delete($data);
    }

    public function countMenu()
    {
        return $this->countAll();
    }
}
