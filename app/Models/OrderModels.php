<?php

namespace App\Models;

use CodeIgniter\Model;

class OrderModels extends Model
{
    protected $table = 'orderan';
    protected $allowedFields = ['id', 'menu_id', 'nomor_meja', 'tgl_order', 'user_id', 'jumlah_item', 'total_harga', 'keterangan'];
    protected $useTimestamps = true;
    protected $primaryKey = 'id';

    public function getOrder()
    {
        return $this->findAll();
    }

    public function countOrder()
    {
        return $this->countAll();
    }

    public function addOrder($data)
    {
        return $this->insertBatch($data);
    }
}
